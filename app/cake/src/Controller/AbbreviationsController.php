<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Abbreviations Controller
 *
 * @property \App\Model\Table\AbbreviationsTable $Abbreviations
 *
 * @method \App\Model\Entity\Abbreviation[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AbbreviationsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize()
    {
        parent::initialize();

        // Set access for public.
        $this->Auth->allow(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Publications']
        ];
        $abbreviations = $this->paginate($this->Abbreviations);

        $this->set(compact('abbreviations'));
    }

    /**
     * View method
     *
     * @param string|null $id Abbreviation id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $abbreviation = $this->Abbreviations->get($id, [
            'contain' => ['Publications'],
        ]);

        $this->set('abbreviation', $abbreviation);
    }
}
