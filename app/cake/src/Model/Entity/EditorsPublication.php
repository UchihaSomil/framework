<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * EditorsPublication Entity
 *
 * @property int $id
 * @property int|null $publication_id
 * @property int|null $editor_id
 * @property int|null $sequence
 *
 * @property \App\Model\Entity\Publication $publication
 * @property \App\Model\Entity\Editor $editor
 */
class EditorsPublication extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'publication_id' => true,
        'editor_id' => true,
        'sequence' => true,
        'publication' => true,
        'editor' => true
    ];
}
