<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Dynasties Model
 *
 * @property \App\Model\Table\ProveniencesTable|\Cake\ORM\Association\BelongsTo $Proveniences
 * @property \App\Model\Table\DatesTable|\Cake\ORM\Association\HasMany $Dates
 * @property \App\Model\Table\RulersTable|\Cake\ORM\Association\HasMany $Rulers
 *
 * @method \App\Model\Entity\Dynasty get($primaryKey, $options = [])
 * @method \App\Model\Entity\Dynasty newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Dynasty[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Dynasty|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Dynasty|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Dynasty patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Dynasty[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Dynasty findOrCreate($search, callable $callback = null, $options = [])
 */
class DynastiesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('dynasties');
        $this->setDisplayField('dynasty');
        $this->setPrimaryKey('id');

        $this->belongsTo('Proveniences', [
            'foreignKey' => 'provenience_id'
        ]);
        $this->hasMany('Dates', [
            'foreignKey' => 'dynasty_id'
        ]);
        $this->hasMany('Rulers', [
            'foreignKey' => 'dynasty_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('polity')
            ->maxLength('polity', 45)
            ->allowEmpty('polity');

        $validator
            ->scalar('dynasty')
            ->maxLength('dynasty', 80)
            ->allowEmpty('dynasty');

        $validator
            ->integer('sequence')
            ->allowEmpty('sequence');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['provenience_id'], 'Proveniences'));

        return $rules;
    }
}
