<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Inscription $inscription
 */
?>

<h3><?= h($inscription->id) ?></h3>
<table class="vertical-table">
    <tr>
        <th scope="row"><?= __('Artifact') ?></th>
        <td><?= $inscription->has('artifact') ? $this->Html->link($inscription->artifact->id, ['controller' => 'Artifacts', 'action' => 'view', $inscription->artifact->id]) : '' ?></td>
    </tr>
    <tr>
        <th scope="row"><?= __('Id') ?></th>
        <td><?= $this->Number->format($inscription->id) ?></td>
    </tr>
    <tr>
        <th scope="row"><?= __('Accepted By') ?></th>
        <td><?= $this->Number->format($inscription->accepted_by) ?></td>
    </tr>
    <tr>
        <th scope="row"><?= __('Update Events Id') ?></th>
        <td><?= $this->Number->format($inscription->update_events_id) ?></td>
    </tr>
    <tr>
        <th scope="row"><?= __('Is Atf2conll Diff Resolved') ?></th>
        <td><?= $inscription->is_atf2conll_diff_resolved ? __('Yes') : __('No'); ?></td>
    </tr>
    <tr>
        <th scope="row"><?= __('Accepted') ?></th>
        <td><?= $inscription->accepted ? __('Yes') : __('No'); ?></td>
    </tr>
    <tr>
        <th scope="row"><?= __('Is Latest') ?></th>
        <td><?= $inscription->is_latest ? __('Yes') : __('No'); ?></td>
    </tr>
</table>
<div class="row">
    <h4><?= __('Atf') ?></h4>
    <?= $this->Text->autoParagraph(h($inscription->atf)); ?>
</div>
<div class="row">
    <h4><?= __('Jtf') ?></h4>
    <?= $this->Text->autoParagraph(h($inscription->jtf)); ?>
</div>
<div class="row">
    <h4><?= __('Transliteration') ?></h4>
    <?= $this->Text->autoParagraph(h($inscription->transliteration)); ?>
</div>
<div class="row">
    <h4><?= __('Transliteration Clean') ?></h4>
    <?= $this->Text->autoParagraph(h($inscription->transliteration_clean)); ?>
</div>
<div class="row">
    <h4><?= __('Tranliteration Sign Names') ?></h4>
    <?= $this->Text->autoParagraph(h($inscription->tranliteration_sign_names)); ?>
</div>
<div class="row">
    <h4><?= __('Annotation') ?></h4>
    <?= $this->Text->autoParagraph(h($inscription->annotation)); ?>
</div>
<div class="row">
    <h4><?= __('Comments') ?></h4>
    <?= $this->Text->autoParagraph(h($inscription->comments)); ?>
</div>
<div class="row">
    <h4><?= __('Structure') ?></h4>
    <?= $this->Text->autoParagraph(h($inscription->structure)); ?>
</div>
<div class="row">
    <h4><?= __('Translation') ?></h4>
    <?= $this->Text->autoParagraph(h($inscription->translation)); ?>
</div>
<div class="row">
    <h4><?= __('Transcription') ?></h4>
    <?= $this->Text->autoParagraph(h($inscription->transcription)); ?>
</div>
<div class="row">
    <h4><?= __('Inscription Comments') ?></h4>
    <?= $this->Text->autoParagraph(h($inscription->inscription_comments)); ?>
</div>
