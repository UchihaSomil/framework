<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsCollection $artifactsCollection
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($artifactsCollection) ?>
            <legend class="capital-heading"><?= __('Edit Artifacts Collection') ?></legend>
            <?php
                echo $this->Form->control('artifact_id', ['options' => $artifacts, 'empty' => true]);
                echo $this->Form->control('collection_id', ['options' => $collections, 'empty' => true]);
            ?>

            <?= $this->Form->submit() ?>
        <?= $this->Form->end() ?>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $artifactsCollection->id],
                ['class' => 'btn-action'],
                ['confirm' => __('Are you sure you want to delete # {0}?', $artifactsCollection->id)]
            )
        ?>
        <br/>
        <?= $this->Html->link(__('List Artifacts Collections'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Artifacts'), ['controller' => 'Artifacts', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Artifact'), ['controller' => 'Artifacts', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Collections'), ['controller' => 'Collections', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Collection'), ['controller' => 'Collections', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
    </div>

</div>
