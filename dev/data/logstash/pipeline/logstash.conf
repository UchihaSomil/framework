input {
  jdbc {
    jdbc_validate_connection => true
    jdbc_driver_class => "Java::org.mariadb.jdbc.Driver"
    jdbc_connection_string => "jdbc:mariadb://mariadb:3306/cdli_db"
    jdbc_user => 'root'
    jdbc_password => ''
    # Run every 10 hour (this can be set according to requirement)
    schedule => "0 */10 * * *"
    #schedule => "*/30 * * * *" # Run every 30 min
    statement => "
    SELECT AP.id, Ar.is_public, AP.artifact_id, AP.publication_id, AP.exact_reference, P.designation, P.bibtexkey, P.year, P.publisher, P.school, P.series, P.title, P.book_title, P.chapter, P.accepted, J.journal, EA.editors_id, EA.editor_authors, AA.authors_id, AA.author_authors
    FROM artifacts_publications as AP
    LEFT JOIN publications AS P
    ON AP.publication_id = P.id
    LEFT JOIN journals AS J
    ON P.journal_id = J.id
    LEFT JOIN
        (
          SELECT EP.publication_id , GROUP_CONCAT(DISTINCT(EP.editor_id)) AS `editors_id`, GROUP_CONCAT(DISTINCT(A.author) SEPARATOR ' ; ') as `editor_authors`
          FROM editors_publications AS EP
          LEFT JOIN authors AS A
          ON EP.editor_id = A.id
          GROUP BY EP.publication_id
        ) AS EA
    ON AP.publication_id = EA.publication_id
    LEFT JOIN
        (
          SELECT AUP.publication_id , GROUP_CONCAT(DISTINCT(AUP.author_id)) AS `authors_id`, GROUP_CONCAT(DISTINCT(A.author) SEPARATOR ' ; ') as `author_authors`
          FROM authors_publications AS AUP
          LEFT JOIN authors AS A
          ON AUP.author_id = A.id
          GROUP BY AUP.publication_id
        ) AS AA
    ON AP.publication_id = AA.publication_id
    LEFT JOIN artifacts AS Ar
    ON AP.artifact_id = Ar.id
    "
    type => "artifacts_publications"
    # sql_log_level => "debug" # ["fatal", "error", "warn", "info", "debug"]
    use_column_value => true
    tracking_column => "artifacts_publications.id"
    # Yet to be set
    record_last_run => true
    # last_run_metadata_path => "/etc/logstash/jdbc_last_run_metadata_path/mariadb"
  }
  jdbc {
    jdbc_validate_connection => true
    jdbc_driver_class => "Java::org.mariadb.jdbc.Driver"
    jdbc_connection_string => "jdbc:mariadb://mariadb:3306/cdli_db"
    jdbc_user => 'root'
    jdbc_password => ''
    # Run every 10 hour (this can be set according to requirement)
    schedule => "0 */10 * * *"
    #schedule => "*/30 * * * *" # Run every 30 min
    statement => "
    SELECT I.id, I.artifact_id, Ar.is_public, Ar.is_atf_public, I.atf, I.translation, I.is_latest
    FROM inscriptions AS I
    LEFT JOIN artifacts AS Ar
    ON I.artifact_id = Ar.id
    "
    type => "inscription"
    # sql_log_level => "debug" # ["fatal", "error", "warn", "info", "debug"]
    use_column_value => true
    tracking_column => "inscriptions.id"
    # Yet to be set
    record_last_run => true
    # last_run_metadata_path => "/etc/logstash/jdbc_last_run_metadata_path/mariadb"
  }
  jdbc {
    jdbc_validate_connection => true
    jdbc_driver_class => "Java::org.mariadb.jdbc.Driver"
    jdbc_connection_string => "jdbc:mariadb://mariadb:3306/cdli_db"
    jdbc_user => 'root'
    jdbc_password => ''
    # Run every 10 hour (this can be set according to requirement)
     schedule => "0 */10 * * *"
    #schedule => "*/30 * * * *" # Run every 30 min
    statement => "
      SELECT A.id, Co.composite_no, A.excavation_no, A.museum_no, A.seal_no, A.accession_no, A.designation AS 'adesignation', A.is_public, A.is_atf_public, A.are_images_public, A.artifact_type_id, AT.artifact_type AS 'atype', Ma.materials, C.collection, A.provenience_id, P.provenience, R.region, A.written_in, A.archive_id, Ar.archive, A.period_id, Pe.period, A.artifact_comments AS 'acomments', Ge.genres, La.languages, I.atf, I.translation, I.transliteration, I.transcription, I.comments AS 'icomments', I.structure, I.annotation, Au.authors, Ed.editors, Pu.publication_id , Pu.publication_type AS 'ptype', Pu.bibtexkey , Pu.designation_exactref , Pu.designation AS 'pdesignation', Pu.year, Pu.title, Pu.school, Pu.book_title , Pu.chapter , Pu.journal , Pu.publisher , Pu.series
      FROM artifacts AS A
      LEFT JOIN artifact_types AS AT
      ON A.artifact_type_id = AT.id
      LEFT JOIN proveniences AS P
      ON A.provenience_id = P.id
      LEFT JOIN regions AS R
      ON P.region_id = R.id
      LEFT JOIN archives AS Ar
      ON A.archive_id = Ar.id
      LEFT JOIN periods AS Pe
      ON A.period_id = Pe.id
      LEFT JOIN (
        SELECT AM.artifact_id,  GROUP_CONCAT(DISTINCT(M.material) SEPARATOR ', ') AS `materials`
        FROM artifacts_materials AS AM
        LEFT JOIN materials AS M
        ON AM.material_id = M.id
        GROUP BY AM.artifact_id
      ) AS Ma
      ON A.id = Ma.artifact_id
      LEFT JOIN (
          SELECT AC.artifact_id,  GROUP_CONCAT(DISTINCT(C.collection) SEPARATOR ', ') AS `collection`
          FROM artifacts_collections AS AC
          LEFT JOIN collections AS C
          ON AC.collection_id = C.id
          GROUP BY AC.artifact_id
      ) AS C
      ON A.id = C. artifact_id
      LEFT JOIN (
          SELECT AG.artifact_id, GROUP_CONCAT(DISTINCT(G.genre) SEPARATOR ' ; ') AS `genres`
          FROM artifacts_genres as AG
          LEFT JOIN genres as G
          ON AG.genre_id = G.id
          GROUP BY AG.artifact_id
      ) AS Ge
      ON A.id = Ge.artifact_id
      LEFT JOIN (
          SELECT AL.artifact_id,
          GROUP_CONCAT(DISTINCT(L.language) SEPARATOR ' ; ') AS `languages`
          FROM artifacts_languages as AL
          LEFT JOIN languages AS L
          ON AL.language_id = L.id
          GROUP BY AL.artifact_id
      ) AS La
      ON A.id = La.artifact_id
      LEFT JOIN (
        SELECT AC.artifact_id, GROUP_CONCAT(AC.composite_no SEPARATOR '\n') as `composite_no`
        FROM artifacts_composites AS AC
        GROUP BY AC.artifact_id
      ) AS Co
      ON A.id = Co.artifact_id
      LEFT JOIN (
          SELECT I.artifact_id,
          GROUP_CONCAT(I.atf SEPARATOR '\n') AS 'atf',
          GROUP_CONCAT(I.translation SEPARATOR '\n') AS 'translation',
          GROUP_CONCAT(I.transliteration SEPARATOR '\n') AS 'transliteration',
          GROUP_CONCAT(I.transcription SEPARATOR '\n') AS 'transcription',
          GROUP_CONCAT(I.comments SEPARATOR '\n') AS 'comments',
          GROUP_CONCAT(I.structure SEPARATOR '\n') AS 'structure',
          GROUP_CONCAT(I.annotation SEPARATOR '\n') AS 'annotation'
          FROM inscriptions as I
          WHERE I.is_latest = 1
          GROUP BY I.artifact_id
      ) AS I
      ON A.id = I.artifact_id
      LEFT JOIN (
          SELECT AUPAP.artifact_id, GROUP_CONCAT(AU.author SEPARATOR '\n' ) AS 'authors'
          FROM (
              SELECT DISTINCT AP.artifact_id, AUP.author_id
              FROM artifacts_publications AS AP
              LEFT JOIN authors_publications AS AUP
              ON AP.publication_id = AUP.publication_id
          ) AS AUPAP
          LEFT JOIN authors AS AU
          ON AUPAP.author_id = AU.id
          GROUP BY AUPAP.artifact_id
      ) As Au
      ON A.id = Au.artifact_id
      LEFT JOIN (
          SELECT AUPEP.artifact_id, GROUP_CONCAT(AU.author SEPARATOR '\n' ) AS 'editors'
          FROM (
              SELECT DISTINCT AP.artifact_id, EP.editor_id
              FROM artifacts_publications AS AP
              LEFT JOIN editors_publications AS EP
              ON AP.publication_id = EP.publication_id
          ) AS AUPEP
          LEFT JOIN authors AS AU
          ON AUPEP.editor_id = AU.id
          GROUP BY AUPEP.artifact_id
      ) as Ed
      ON A.id = Ed.artifact_id
      LEFT JOIN (
          SELECT AP.artifact_id,
          GROUP_CONCAT(DISTINCT(AP.publication_id) SEPARATOR ' ') AS 'publication_id',
          GROUP_CONCAT(DISTINCT(AP.publication_type) SEPARATOR ' ') AS 'publication_type',
          GROUP_CONCAT(DISTINCT(P.bibtexkey) SEPARATOR ' \n ') AS 'bibtexkey',
          GROUP_CONCAT(CONCAT_WS(' ',P.designation, AP.exact_reference) SEPARATOR ' \n ') AS 'designation_exactref',
          GROUP_CONCAT(DISTINCT(P.designation) SEPARATOR ' \n ') AS 'designation',
          GROUP_CONCAT(DISTINCT(P.year) SEPARATOR ' ') AS 'year',
          GROUP_CONCAT(DISTINCT(P.title) SEPARATOR ' \n ') AS 'title',
          GROUP_CONCAT(DISTINCT(P.school) SEPARATOR ' \n ') AS 'school',
          GROUP_CONCAT(DISTINCT(P.book_title) SEPARATOR ' \n ') AS 'book_title',
          GROUP_CONCAT(DISTINCT(P.chapter) SEPARATOR ' ') AS 'chapter',
          GROUP_CONCAT(DISTINCT(J.journal) SEPARATOR ' \n ') AS 'journal',
          GROUP_CONCAT(DISTINCT(P.publisher) SEPARATOR ' \n ') AS 'publisher',
          GROUP_CONCAT(DISTINCT(P.series) SEPARATOR ' \n ') AS 'series'
          FROM artifacts_publications AS AP
          LEFT JOIN publications AS P
          ON AP.publication_id = P.id
          LEFT JOIN journals AS J
          ON P.journal_id = J.id
          GROUP BY AP.artifact_id
      ) AS Pu
      ON A.id = Pu.artifact_id
    "
    type => "advanced_artifacts"
    # sql_log_level => "debug" # ["fatal", "error", "warn", "info", "debug"]
    use_column_value => true
    tracking_column => "advanced_artifacts.id"
    # Yet to be set
    record_last_run => true
    # last_run_metadata_path => "/etc/logstash/jdbc_last_run_metadata_path/mariadb"
  }
}

output {
  stdout {
    codec => json_lines
  }
  elasticsearch {
    hosts => ["elasticsearch:9200"]
    index => "%{type}"
    user => "elastic"
    password  => "elasticchangeme"
    document_id => "%{id}"
  }
}
